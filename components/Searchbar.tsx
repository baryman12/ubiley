import Link from "next/link";
import { useEffect, useMemo, useState } from "react";
import { ScrollArea } from "@/components/ui/scroll-area";
import { IGuest } from "@/types";

const Searchbar = () => {
  const [guests, setGuests] = useState<IGuest[]>([]);
  const [search, setSearch] = useState("");
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    fetch(`/api/guests`)
      .then((res) => res.json())
      .then((data) => {
        setGuests(data);
        setLoading(false);
      });
  }, []);

  const handleChange = (e: any) => {
    setSearch(e.target.value);
  };

  const results = useMemo(() => {
    if (!guests || search.length < 2) return [];
    /*    const filtered = guests.filter((guest) => {
      return guest.fullname.toLowerCase().includes(search.toLowerCase());
    }); */
    const filteredRegexp = guests.filter((guest) => {
      const regexp = new RegExp(search, "i");
      return regexp.test(guest.fullname);
    });

    return filteredRegexp;
  }, [guests, search]);

  return (
    <div className="mb-12 min-h-[60vh]">
      <form className="flex w-full">
        <input type="text" placeholder="Фамилия Имя" onChange={handleChange} value={search} className="text-lg border-b-2 bg-transparent border-neutral-300 placeholder:text-neutral-300 flex-1 w-full py-2 px-2 focus:outline-none focus:border-primary" />
      </form>
      {results.length > 0 && (
        <ScrollArea className="mt-2 h-[50vh] w-full">
          <div className="flex flex-col gap-1">
            {results.map((result: IGuest) => (
              <Link key={result.id} href={`/guest/${result.id}`} className="text-2xl text-neutral-800 text-title py-2 px-2">
                {result.fullname}
              </Link>
            ))}
          </div>
        </ScrollArea>
      )}
    </div>
  );
};

/* export const getStaticPaths = async () => {
  const res = await fetch("/guests.json");
  const guests = await res.json();

  return { guests, fallback: false };
};
 */
export default Searchbar;
