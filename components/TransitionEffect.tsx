import { motion, AnimatePresence } from "framer-motion";
import { useRouter } from "next/router";
import { FC } from "react";

const variants = {
  fadeIn: {
    y: 100,
    opacity: 0,
    transition: {
      duration: 1,
      ease: "easeInOut",
    },
  },
  inactive: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 1,
      ease: "easeInOut",
    },
  },
  fadeOut: {
    opacity: 0,
    y: -100,
    transition: {
      duration: 1,
      ease: "easeInOut",
    },
  },
};

/*
 * Read the blog post here:
 * https://letsbuildui.dev/articles/animated-page-transitions-in-nextjs
 */
const TransitionEffect: FC<any> = ({ children }) => {
  const { asPath } = useRouter();

  return (
    <AnimatePresence initial={false} mode="wait">
      <motion.div key={asPath} variants={variants} initial="fadeIn" animate="inactive" exit="fadeOut">
        {children}
      </motion.div>
    </AnimatePresence>
  );
};

export default TransitionEffect;
