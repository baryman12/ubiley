export interface IGuest {
  id: string;
  fullname: string;
  table: number;
}
