import "@/styles/globals.css";
import type { AppProps } from "next/app";
import TransitionEffect from "../components/TransitionEffect";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <TransitionEffect>
      <Component {...pageProps} />
    </TransitionEffect>
  );
}
