import { motion } from "framer-motion";
import Image from "next/image";
import Router from "next/router";

export default function Home() {
  /* useEffect(() => {
    setTimeout(() => {
      Router.push("/guest");
    }, 1750);
  }, []); */

  return (
    <main className="flex min-h-screen flex-col justify-end items-start relative" style={{ background: "url('/noise.png')", backgroundSize: "100px", backgroundRepeat: "repeat" }}>
      <div className="absolute inset-0 overflow-hidden">
        <motion.div
          className="absolute inset-0 origin-right right-[-280px]"
          initial={{ x: 50, opacity: 0 }}
          animate={{ x: 0, rotate: -35, opacity: 1 }}
          transition={{
            type: "spring",
            stiffness: 20,
          }}
          onAnimationComplete={() => {
            setTimeout(() => {
              Router.push("/guest");
            }, 100);
          }}
        >
          <Image className="absolute top-[-200px] right-[-200px]" alt="flower-1" width={628} height={1136} src={"/flower-01.png"} priority />
        </motion.div>
      </div>
      <motion.div
        style={{ originX: 0.25 }}
        className="mb-12 px-6"
        initial={{ y: -100, scale: 0.75, opacity: 0 }}
        animate={{ y: 0, scale: 1, opacity: 1 }}
        transition={{
          type: "spring",
          stiffness: 50,
        }}
      >
        <h1 className="font-title text-4xl text-primary mb-1">Добро пожаловать!</h1>
        <motion.p
          initial={{ x: -50, scale: 0.75, opacity: 0 }}
          animate={{ x: 0, scale: 1, opacity: 1 }}
          transition={{
            type: "spring",
            stiffness: 75,
          }}
          className="text-lg mb-12"
        >
          Юбилей Саргыланы Семеновны
        </motion.p>
        <motion.p
          initial={{ x: -100, scale: 0.75, opacity: 0 }}
          animate={{ x: 0, scale: 1, opacity: 1 }}
          transition={{
            type: "spring",
            stiffness: 75,
          }}
          className="font-numeric text-3xl text-neutral-300"
        >
          24.06.2023 17:00
        </motion.p>
      </motion.div>
    </main>
  );
}
