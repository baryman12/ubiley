import Searchbar from "@/components/Searchbar";

export default function FioPage() {
  return (
    <main className="flex min-h-screen flex-col justify-end relative px-6" style={{ background: "url('/noise.png')", backgroundSize: "100px", backgroundRepeat: "repeat" }}>
      <h1 className="font-title text-primary text-2xl mb-2">
        Пожалуйста,
        <br />
        найдите себя в списке
      </h1>

      <Searchbar />
    </main>
  );
}
