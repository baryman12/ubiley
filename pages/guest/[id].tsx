import { IGuest } from "@/types";
import Image from "next/image";
import guests from "../api/guests.json";

export const getStaticProps = async ({ params }: any) => {
  // const guests = await import("../api/guests.json");

  const guest = guests.find((user) => user.id === params.id);

  return {
    props: {
      guest,
      guests,
    },
  };
};

export const getStaticPaths = async () => {
  // const guests = await import("../api/guests.json");

  const paths = guests.map((guest) => ({
    params: { id: guest.id },
  }));

  return { paths, fallback: false };
};

export default function GuestPage({ guest, guests }: { guest: IGuest; guests: IGuest[] }) {
  const guestsByTable = guests.reduce((acc, guest) => {
    if (!acc[guest.table]) {
      acc[guest.table] = [];
    }
    acc[guest.table].push(guest);
    return acc;
  }, {} as { [key: string]: IGuest[] });

  return (
    <main className="flex min-h-screen flex-col relative" style={{ background: "url('/noise.png')", backgroundSize: "100px", backgroundRepeat: "repeat" }}>
      <section className="relative min-h-screen pt-12 overflow-hidden px-6">
        <h1 className="font-title text-primary text-2xl mb-6">
          {guest.fullname},
          <br />
          <span className="text-xl text-neutral-900">добро пожаловать</span>
        </h1>
        <p className="text-lg">
          Я очень рада видеть вас на вечере, посвященном моему дню рождения.Это для меня особый день!
          <br />
          Хочу поблагодарить каждого из Вас за то, что Вы пришли разделить со мной этот праздник.
        </p>
        <div className="relative right-[75px] w-[707px] h-[707px]">
          <Image className="absolute w-[707px] h-auto z-10" src="/photo-1.png" width={700} height={700} alt="photo" priority />
          <Image className="absolute top-[-30px] left-[150px] transform -rotate-45 z-0" alt="flower-1" width={300} height={300} src={"/flower-01.png"} priority />
        </div>
      </section>
      <section className="relative pt-6 pb-12 px-6">
        <p className="text-lg text-neutral-500">Я очень ценю Вашу дружбу и поддержку.</p>
        <p className="py-6 mt-12 mx-12 text-center text-primary border-y border-neutral-300 text-4xl font-title mb-12">Спасибо!</p>
        <p className="text-lg text-neutral-500">Желаю всем Вам отличного настроения, приятного времяпровождения и незабываемых впечатлений от этого вечера!</p>
      </section>
      <section className="flex flex-col relative pb-16">
        <h1 className="text-2xl font-title text-center mb-4">Рассадка гостей</h1>
        <figure className="flex flex-col mx-4 p-4 bg-white shadow-2xl rounded-xl">
          <div className="flex flex-col text-center border border-primary rounded-md">
            <h2 className="font-title my-4 text-primary text-xl">
              <span className="text-neutral-800">
                {guest.fullname.split(" ")[1]},<br />
              </span>
              Ваш стол №{guest.table}
            </h2>
            <p className="text-neutral-500 text-lg mb-2">Также за Вашим столиком:</p>
            <ul className="text-center mb-2">
              {guestsByTable[guest.table]
                .filter((g) => g.id !== guest.id)
                .map((guest) => (
                  <li key={guest.id} className="text-center">
                    <span className="text-lg">{guest.fullname}</span>
                  </li>
                ))}
            </ul>
            {/*  {Object.keys(guestsByTable).map((table) => (
              <div key={table} className="flex flex-col">
                <h3 className="font-title my-4 text-primary text-xl">Стол №{table}</h3>
                <ul className="flex flex-col">
                  {guestsByTable[table].map((guest) => (
                    <li key={guest.id} className="flex flex-row justify-between">
                      <span className="text-lg">{guest.fullname}</span>
                    </li>
                  ))}
                </ul>
              </div>
            ))} */}
          </div>
        </figure>
      </section>
      <section className="flex flex-col relative pb-16">
        <h2 className="text-2xl font-title text-center mb-6">Для вас будут выступать</h2>
        <div className="flex flex-col px-6">
          <div className="flex flex-col items-center justify-center text-center mb-12">
            <Image src={"/guest-1.jpg"} className="rounded-full overflow-hidden h-[255px] w-[200px] object-cover" width={200} height={200} alt="1" />
            <h2 className="font-title text-primary text-xl my-3">Адамова Айталина Саввична</h2>
            <p className="text-lg text-neutral-500">Советская и российская артистка-вокалистка Государственного театра оперы и балета Республики Саха (Якутия) им. Д. К. Сивцева-Суорун Омоллоона</p>
          </div>
          <div className="flex flex-col items-center justify-center text-center mb-12">
            <Image src={"/guest-2.jpg"} className="rounded-full overflow-hidden h-[255px] w-[200px] object-cover" width={200} height={200} alt="1" />
            <h2 className="font-title text-primary text-xl my-3">Ансамбль «Аар - Аартык»</h2>
            <p className="text-lg text-neutral-500">Многократные лауреаты Международных фольклорных фестивалей в Европе и России</p>
          </div>
          <div className="flex flex-col items-center justify-center text-center mb-12">
            <Image src={"/guest-3.jpg"} className="rounded-full overflow-hidden h-[255px] w-[200px] object-cover" width={200} height={200} alt="1" />
            <h2 className="font-title text-primary text-xl my-3">Клавдия и Герман Хатылаевы</h2>
            <p className="text-lg text-neutral-500">Народные артисты РС(Я)</p>
          </div>
          <div className="flex flex-col items-center justify-center text-center mb-12">
            <Image src={"/guest-4.jpg"} className="rounded-full overflow-hidden h-[255px] w-[200px] object-cover" width={200} height={200} alt="1" />
            <h2 className="font-title text-primary text-xl my-3">Емельянов Александр Георгиевич</h2>
            <p className="text-lg text-neutral-500">Солист Государственного театра оперы и балета, народный артист Якутии</p>
          </div>
          <div className="flex flex-col items-center justify-center text-center mb-12">
            <Image src={"/guest-5.jpg"} className="rounded-full overflow-hidden h-[255px] w-[200px] object-cover" width={200} height={200} alt="1" />
            <h2 className="font-title text-primary text-xl my-3">Тихонова Розалия Михайловна</h2>
            <p className="text-lg text-neutral-500">Руководитель департамента культуры Нюрбинского улуса.</p>
          </div>
        </div>
      </section>
      <section className="flex flex-col relative pb-16">
        <h2 className="text-2xl font-title text-center mb-6">Вступайте в группу</h2>
        <a className="flex flex-col gap-4 justify-center items-center text-primary" href="https://chat.whatsapp.com/K7ixbi3WeHoH2cLLAx1A22" target="_blank">
          <img className="h-32 w-32" src="/whatsapp.svg" alt="whatsapp" />
          <h3 className="text-3xl font-bold">Whats app</h3>
        </a>
      </section>
    </main>
  );
}
